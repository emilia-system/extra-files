# So, you want to help the Emilia Project?

To contribute, first you need to fork the project. Go to the project's git page, and choose the one you want to contribute. It may be this very project, so let's begin!

There is a bit of reading about the contribution you need to make.

## The project guidelines

Emilia is a project made with love. The project is not interested in money or fame, being 
interested only in making the best system for everyone, being easy enough to use, giving full
freedom and control of your computer and teaching the linux way of doing things efficiently.

If you share the vision and liked the system, you are in the right place.

### Code of Conduct

We as a team have few rules, about the project itself. But that does not mean you have to be mean.
Please follow the following rules:

1. Be nice. Read your messages for yourself and see if it's fair to say what you are going to say.
2. No politics or off topic opinions (like religions). Here is a place for code, and not for silly thinking.
3. No swearing in the code. If you are angry, go out and eat a large sanduiche and leave the code alone!
4. No need to worry about shame while asking questions. Silly questions happens all the time! 
Everyone was new to coding and topics once.
5. No blaming! Leave that to Git, and someone could have made a mistake/oversight

Also note, that while other projects have like 20 paragraphs of text just to say similar things, the Emilia Team likes to keep things simple

### Note :

Rule 34 with the Character Emilia is insta-ban. The creator loves her too much.

## Topics

### The Arts

Emilia as a character is complicated. The project in the moment is in need of someone to draw her, as
the Project leader, the creator, has no drawing cappacity.
If you are interested in drawing her, send us a email or open a issue in emilia-arts.

### Translations

To translate a project, you must see the folder 'po' inside then. You are likelly to find a file named 
emilia-<project>.pot, and this is the one you need. Create a folder over there with the language 
desired and copy the .pot file to the new folder, and rename so the extension is .po instead of .pot.

After that, check the project's Makefile and search the update_language (or similar) for the 
installation of the language. Just copy from a existing one and change it to your language.

Also, please, use UTF-8 everywhere.

### Packaging

Emilia borned in the Arch Linux world. To understand how to do it, we suggest a reading
[on Arch's guidelines](https://wiki.archlinux.org/index.php/Arch_package_guidelines). 
Who could understand pacman and the best way to create packaging than the creator?

The Emilia adds uppon this rules, adding some guidelines.

For new packages, the groups:

1. The `emilia-base` group is used to the packages that are absolutelly necessary for the OS to be considered a Emilia System
2. The `emilia-themes` group is used for themes (GTK, Qt, and others) and arts, like wallpapers.
3. The `emilia` group should have every package that a common user should have. 
It's basically everyone except emiso and calamares

For old packages:

1. Check dependencies from time to time. Arch updates it's packages, and sometimes, emilia's packages need recompiling. Like calamares, or garden.

