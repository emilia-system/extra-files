function init() {
	
	var canvas = document.getElementsByTagName("canvas")[0];
	//var c = canvas.getContext("2d");
	
	var c = C2S(1300,640)
	
	//Curva de rosa
	c.beginPath();
	c.strokeStyle = 'rgb(255, 145, 28)';
	
	c.lineWidth = 25;
	//c.shadowBlur = 20;
	//c.shadowColor = 'rgb(249, 151, 47)';
	c.moveTo(950,320);
	
	for(var angle = 0; angle <= 2*Math.PI+0.1; angle += 0.01){
		var x = 650 + 300 * Math.cos(4 * angle) * Math.cos(angle);
		var y = 320 + 300 * Math.cos(4 * angle) * Math.sin(angle); 
		
		c.lineTo(x,y);
	}
	
	c.stroke();
	
	c.fillStyle = 'rgb(255,145,28)';
	//c.fill();
	c.closePath();
	
	//circulo maior, o aro
	c.beginPath();
	
	c.fillStyle = '#f92222';
	c.strokeStyle = '#ff3b3b';
	
	c.lineWidth = 10;
	//c.shadowBlur = 15;
	//c.shadowColor = '#ff2b2b';
	
	c.moveTo(700,320);
	c.lineWidth = 6;
	for(var angle = 0; angle <= 2*Math.PI; angle += 0.01){
		var x = 600 + 100 * Math.cos(angle) * Math.cos(angle);
		var y = 320 + 100 * Math.cos(angle) * Math.sin(angle); 
		
		c.lineTo(x,y);
	}
	c.fill();
	c.stroke();
	c.closePath();
	
	/*
	//PEQUENO CIRCULO
	c.beginPath();
	c.fillStyle = '#f92222';
	c.strokeStyle = '#ff2b2b';
	
	c.lineWidth = 1;
	//c.shadowBlur = 4;
	c.shadowColor = '#ff2b2b';
	
	c.moveTo(650,320);
	c.lineWidth = 10;
	for(var angle = 0; angle <= 2*Math.PI; angle += 0.01){
		var x = 640 + 20 * Math.cos(angle) * Math.cos(angle);
		var y = 320 + 20 * Math.cos(angle) * Math.sin(angle); 
		
		c.lineTo(x,y);
	}
	c.fill();
	c.stroke();
	c.closePath();
	*/
	
	//var img = new Image();
	//img.src = canvas.toDataURL();
	//document.body.appendChild(img);
	
	
	document.getElementById("saida").innerHTML = c.getSerializedSvg(true);
	
	
}

//https://www.sitepoint.com/delay-sleep-pause-wait/
function Sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}