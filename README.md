# Emilia Extra Files

Just auxiliary files and administration work. Nothing really important for the other projects

## Project Status - Pipelines

- Sorry guys, pipeline is now limited as of 10/2020. So they are going to be disabled as it's not that viable anymore...

| Project          | Package group(s) | Licensing | Pipeline master | Pipeline dev (if exists)|
| :--------------  | :--------------- | :-------- | :-------------- | :---------------------- |
| Emiso            | _none_           |[![License](https://img.shields.io/badge/license-GPL3-red)](https://choosealicense.com/licenses/gpl-3.0/)   |[![pipeline status](https://gitlab.com/emilia-system/emiso/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emiso/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emiso/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emiso/commits/dev)|
| Tools            | emilia, base     |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-tools/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-tools/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-tools/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-tools/commits/dev)|
| Calamares        | _none_           |[![License](https://img.shields.io/badge/license-GPL3-red)](https://choosealicense.com/licenses/gpl-3.0/)   |[![pipeline status](https://gitlab.com/emilia-system/emilia-calamares/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-calamares/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-calamares/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-calamares/commits/dev)|
| Garden           | emilia, base     |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-garden/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-garden/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-garden/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-garden/commits/dev)|
| Core             | emilia, base     |[![License](https://img.shields.io/badge/license-GPL3-red)](https://choosealicense.com/licenses/gpl-3.0/)   |[![pipeline status](https://gitlab.com/emilia-system/emilia-core/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-core/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-core/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-core/commits/dev)|
| Meta-packages    | meta             |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-meta-packages/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-meta-packages/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-meta-packages/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-meta-packages/commits/dev)|
| Dotfiles         | emilia, base     |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-dotfiles/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-dotfiles/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-dotfiles/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-dotfiles/commits/dev)|
| Manual           | emilia, base     |[![License](https://img.shields.io/badge/license-CCPL-blue)](https://choosealicense.com/licenses/cc-by-4.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-manual/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-manual/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-manual/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-manual/commits/dev)|
| Daisy and Themes | emilia, themes   |[![License](https://img.shields.io/badge/license-GPL2-yellow)](https://choosealicense.com/licenses/gpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-themes/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-themes/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-themes/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-themes/commits/dev)|
| Grub theme       | emilia, themes   |[![License](https://img.shields.io/badge/license-GPL3-red)](https://choosealicense.com/licenses/gpl-3.0/)   |[![pipeline status](https://gitlab.com/emilia-system/emilia-grub/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-grub/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-grub/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-grub/commits/dev)|
| Arts             | emilia, base     |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-arts/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-arts/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-arts/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-arts/commits/dev)|
| Docs (doxygen)   | emilia           |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-docs/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-docs/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-docs/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-docs/commits/dev)|
| aLive (planning) | real     |[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)|[![pipeline status](https://gitlab.com/emilia-system/emilia-alive/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-alive/commits/master)|[![pipeline status](https://gitlab.com/emilia-system/emilia-alive/badges/dev/pipeline.svg)](https://gitlab.com/emilia-system/emilia-alive/commits/dev)|

### The groups

- _emilia_ is the main group. _emilia_ contains every package the final user should have (everyone except emiso, calamares, meta and the optional assistant)
- _base_ is emilia-base. Default Emilia customization, no extras.
- _themes_ is emilia-themes. Contains extra, not important themes to customize Emilia.
- _meta_ is emilia-meta. Just a handfull of meta packages about Desktops and their apps. Make compilling Emilia easier and installing a Desktop more simple.
- _real_ is emilia-real. The personal assistant and the custom background. For now they are only dreams, but maybe one day...

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

# Emilia Extra Files - pt_BR

Só arquivos auxiliares e trabalho de administração. Nada importante de verdade para os outros projetos.

### Os grupos

- _emilia_ é o grupo principal. _emilia_ contém todo pacote que o usuário final deveria ter (todos exceto emiso, calamares, meta e o assistente opcional)
- _base_ é o emilia-base. Default Emilia customization, no extras.
- _themes_ é o emilia-themes. Contains extra, not important themes to customize Emilia.
- _meta_ é o emilia-meta. Apenas uma coleção de meta packages sobre Desktops e suas aplicações. Fazem a compilação da Emilia ser mais facil e a instalação de Desktops mais simples.
- _real_ é o emilia-real. O assistente pessoal e o fundo customizado. Por enquanto eles são apenas sonhos, mas talvez um dia...

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)
