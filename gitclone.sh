#!/bin/bash

pkgs=()

pkgs+=("emilia-garden")
pkgs+=("emilia-arts")
pkgs+=("emilia-grub")
pkgs+=("emilia-core")
pkgs+=("emilia-meta-packages")
pkgs+=("emilia-dotfiles")
pkgs+=("emilia-tools")
# pkgs+=("emilia-manpages")
pkgs+=("emilia-docs")
pkgs+=("emilia-manual")
pkgs+=("emilia-calamares")
pkgs+=("emilia-themes")
pkgs+=("emiso")
pkgs+=("extra-files")
# pkgs+=("official-repository")

for _pkg in "${pkgs[@]}"; do
    git clone https://gitlab.com/emilia-system/${_pkg}.git
done
