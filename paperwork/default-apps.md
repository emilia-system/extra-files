# Default applications.

Desktops abreviations are the same as in Emiso. To recap:

 - N - Minimal/NetInstall. Same as CLI actually.
 - G - GNOME
 - K - KDE-Plasma 5
 - X - XFCE 4
 - C - Cinnamon
 - M - MATE
 - L - LXQT
 - B - Budgie

| Desktops | Functionality    | Chosen-app       | Toolkit   | Status |
| :------- | :--------------  | :--------------- | :-------- | :----- |
| Everyone | Music Playing    | Lollypop         | GTK Gnome | Nice!  |
